package com.cc.tidjean.countmycigarettes;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.gson.Gson;

import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tidjean on 14/03/17.
 */

public class dataSql extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 7;
    private static final String DICTIONARY_TABLE_NAME = "cigarette";
    private static final String DICTIONARY_TABLE_CREATE =
            "CREATE TABLE " + DICTIONARY_TABLE_NAME + " (time TEXT, timestamp TEXT, string email)";

    dataSql(Context context) {
        super(context, "CigaretteCount", null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DICTIONARY_TABLE_CREATE);
        db.execSQL("CREATE TABLE config (price TEXT, currency TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("ALTER TABLE cigarette ADD COLUMN email string");
    }

    public List getAll(SQLiteDatabase database){
        String[] cols = new String[] {"time", "timestamp"};
        Cursor mCursor = database.query(true, "cigarette",cols,null
                , null, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }

        List cigarettes = new ArrayList<Object>();
        if (mCursor.moveToFirst()) {
            do {
                Cigarette cigarette = new Cigarette();
                cigarette.time = mCursor.getString(0);
                cigarette.timestamp = mCursor.getString(1);

                cigarettes.add(cigarette);
            } while (mCursor.moveToNext());
        }
        return cigarettes;
    }

    public Configuration getConfig(SQLiteDatabase database) {
        Configuration config = new Configuration();
        String[] cols = new String[] {"price", "currency"};
        Cursor mCursor = database.query(true, "config",cols,null
                , null, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }

        List cigarettes = new ArrayList<Object>();
        if (mCursor.moveToFirst()) {
            do {
                config.price= Double.parseDouble(mCursor.getString(0));
                config.currency = mCursor.getString(1);
            } while (mCursor.moveToNext());
        }
        return config;
    }

    public void setConfig (SQLiteDatabase database, Configuration config) {
        database.execSQL("DROP TABLE config");
        database.execSQL("CREATE TABLE config (price TEXT, currency TEXT)");
        ContentValues values = new ContentValues();
        values.put("price", config.price.toString());
        values.put("currency", config.currency);
        database.insert("config", null, values);
    }
    public class Configuration {
        Double price;
        String currency;

        public Configuration() {
            this.price = 0.0;
            this.currency = "";
        }
    }

    public class Cigarette {
        String time;
        String timestamp;
        String email;
    }

    public void addCigarette (SQLiteDatabase database, final Timestamp time, final GoogleSignInAccount account) {
        ContentValues values = new ContentValues();
        values.put("time", time.toString());
        values.put("timestamp", time.getTime());
        if (account != null) {
            values.put("email", account.getEmail());
            Log.w("email", account.getEmail());
        }
        database.insert("cigarette", null, values);

        //if user is logged
        if (account != null) {
            String id = account.getId();
            Cigarette cigarette = new Cigarette();
            cigarette.time = time.toString();
            cigarette.timestamp = time.getTime() + "";
            cigarette.email = account.getEmail();
            List cigarettes = new ArrayList<Object>();
            cigarettes.add(cigarette);
            String postData = new Gson().toJson(cigarettes);
            SendDeviceDetails post = new SendDeviceDetails();
            post.execute("http://countmycigarettes.com/sync/" + id, postData.toString());
        }
    }

    public void syncFromServer (SQLiteDatabase database, GoogleSignInAccount account) {
        if (account != null) {
            String id = account.getId();

            ArrayList<dataSql.Cigarette> cigarettes = (ArrayList<dataSql.Cigarette>) getAll(database);

            String postData = new Gson().toJson(cigarettes);
            SendDeviceDetails post = new SendDeviceDetails();
            post.execute("http://countmycigarettes.com/sync/" + id, postData.toString());

            GetDeviceDetails get = new GetDeviceDetails();
            get.execute("http://countmycigarettes.com/sync/" + id);
        }
    }

    private class SendDeviceDetails extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {

            String data = "";

            HttpURLConnection httpURLConnection = null;
            try {

                httpURLConnection = (HttpURLConnection) new URL(params[0]).openConnection();
                httpURLConnection.setRequestMethod("POST");

                httpURLConnection.setDoOutput(true);

                Log.w("params::::::::", params[1]);
                DataOutputStream wr = new DataOutputStream(httpURLConnection.getOutputStream());
                wr.writeBytes("PostData=[" + params[1] + "]");
                wr.flush();
                wr.close();

                InputStream in = httpURLConnection.getInputStream();
                InputStreamReader inputStreamReader = new InputStreamReader(in);

                int inputStreamData = inputStreamReader.read();
                while (inputStreamData != -1) {
                    char current = (char) inputStreamData;
                    inputStreamData = inputStreamReader.read();
                    data += current;
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (httpURLConnection != null) {
                    httpURLConnection.disconnect();
                }
            }

            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.e("TAG", result); // this is expecting a response code to be sent from your server upon receiving the POST data
        }
    }

    private class GetDeviceDetails extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {

            String data = "";

            HttpURLConnection httpURLConnection = null;
            try {

                httpURLConnection = (HttpURLConnection) new URL(params[0]).openConnection();
                httpURLConnection.setRequestMethod("GET");

                InputStream in = httpURLConnection.getInputStream();
                InputStreamReader inputStreamReader = new InputStreamReader(in);

                int inputStreamData = inputStreamReader.read();
                while (inputStreamData != -1) {
                    char current = (char) inputStreamData;
                    inputStreamData = inputStreamReader.read();
                    data += current;
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (httpURLConnection != null) {
                    httpURLConnection.disconnect();
                }
            }

            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.e("TAG_GET", result); // this is expecting a response code to be sent from your server upon receiving the POST data
            Gson gson = new Gson();
            Cigarette[] listCigarettes = gson.fromJson(result, Cigarette[].class);
            SQLiteDatabase writableDatabase = getWritableDatabase();
            ArrayList<dataSql.Cigarette> cigarettesDb = (ArrayList<Cigarette>) getAll(writableDatabase);

            if (listCigarettes != null) {
                for (int i = 0; i < listCigarettes.length; i++) {
                    boolean test = true;
                    for (int j = 0; j < cigarettesDb.size(); j++) {
                        if (cigarettesDb.get(j).time == listCigarettes[i].time && cigarettesDb.get(j).timestamp == listCigarettes[i].timestamp) {
//                        Log.w("Test", "+++++" + cigarettesDb.get(j).time);
                            test = false;

                        }
                    }

                    if (test) {
                        ContentValues values = new ContentValues();
                        values.put("time", listCigarettes[i].time);
                        values.put("timestamp", listCigarettes[i].timestamp);
                        writableDatabase.insert("cigarette", null, values);
                    }
                }
            }
        }
    }
}