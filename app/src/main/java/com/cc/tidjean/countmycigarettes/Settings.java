package com.cc.tidjean.countmycigarettes;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

/**
 * Created by tidjean on 26/03/17.
 */

public class Settings extends AppCompatActivity {
    private Context context;
    private SQLiteDatabase database;
    private dataSql dbHelper;
    private dataSql.Configuration config;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getApplicationContext();
        this.dbHelper = new dataSql(context);
        this.database = dbHelper.getWritableDatabase();

        this.config = dbHelper.getConfig(database);

        setContentView(R.layout.settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.home);
        setSupportActionBar(toolbar);

        Spinner dropdown = (Spinner)findViewById(R.id.devises);
        String[] items = new String[]{"$", "€", "RMB"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, items);
        dropdown.setAdapter(adapter);
        dropdown.setSelection(adapter.getPosition(config.currency));

        EditText price = (EditText)findViewById(R.id.valuePrice);
        price.setText(config.price.toString());

        Button viewDay = (Button) findViewById(R.id.saveconfig);
        viewDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText price = (EditText) findViewById(R.id.valuePrice);
                String priceValue = price.getText().toString();
                Spinner devise = (Spinner) findViewById(R.id.devises);
                String deviceC = devise.getSelectedItem().toString();
                config.price = Double.parseDouble(priceValue);
                config.currency = deviceC;
                dbHelper.setConfig(database, config);
                //display confirmation message
                CharSequence text = "Configuration saved";
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
                Intent myIntent = new Intent(context, MainActivity.class);
                startActivity(myIntent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                // User chose the "Settings" item, show the app settings UI...
                Intent settings = new Intent(context, Settings.class);
                startActivity(settings);
                return true;

            case R.id.dashboard:
                Intent dashboard = new Intent(context, MainActivity.class);
                startActivity(dashboard);
                return true;

            case android.R.id.home:
                Intent home = new Intent(context, MainActivity.class);
                startActivity(home);
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }
}
