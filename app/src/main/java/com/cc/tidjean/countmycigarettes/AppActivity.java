package com.cc.tidjean.countmycigarettes;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import java.sql.Timestamp;

public class AppActivity extends AppCompatActivity {

    protected SQLiteDatabase database;
    protected Context context;
    protected dataSql.Configuration config;
    protected dataSql dbHelper;
    protected GoogleSignInClient mGoogleSignInClient;
    protected GoogleSignInAccount account;
    private int mYear, mMonth, mDay, mHour, mMinute;
    public static int RC_SIGN_IN = 100;
    protected DrawerLayout mDrawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        this.context = getApplicationContext();
        this.dbHelper = new dataSql(context);
        this.database = dbHelper.getWritableDatabase();
        this.config = dbHelper.getConfig(database);

        final android.icu.util.Calendar c = android.icu.util.Calendar.getInstance();
        mHour = c.get(android.icu.util.Calendar.HOUR_OF_DAY);
        mMinute = c.get(android.icu.util.Calendar.MINUTE);
        mYear = c.get(android.icu.util.Calendar.YEAR);
        mMonth = c.get(android.icu.util.Calendar.MONTH);
        mDay = c.get(android.icu.util.Calendar.DAY_OF_MONTH);

        final TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        mHour = hourOfDay;
                        mMinute = minute;

                        String mmMonth, mmDay, mmHour, mmMinute;

                        if (mMonth < 10) {
                            mmMonth = "0" + mMonth;
                        } else {
                            mmMonth = "" + mMonth;
                        }
                        if (mDay < 10) {
                            mmDay= "0" + mDay;
                        } else {
                            mmDay = "" + mDay;
                        }
                        if (mHour < 10) {
                            mmHour = "0" + mHour;
                        } else {
                            mmHour = "" + mHour;
                        }
                        if (mMinute < 10) {
                            mmMinute = "0" + mMinute;
                        } else {
                            mmMinute = "" + mMinute;
                        }

                        //store the time into the database
                        ContentValues values = new ContentValues();

                        String input = mYear + "-" + mmMonth + "-" + mmDay + " " + mmHour + ":" + mmMinute + ":00";
                        Log.e("----------", input);
                        Timestamp time = java.sql.Timestamp.valueOf( input ) ;
                        dbHelper.addCigarette(database, time, account);

                        //display confirmation message
                        CharSequence text = context.getText(R.string.added);
                        int duration = Toast.LENGTH_SHORT;
                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();

                        display();
                    }
                }, mHour, mMinute, false);

        final DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        mYear = year;
                        mMonth = monthOfYear + 1;
                        mDay = dayOfMonth;

                        // Launch Time Picker Dialog
                        timePickerDialog.show();
                    }
                }, mYear, mMonth, mDay);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerDialog.show();
            }
        });

        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        //setup the left menu
        mDrawerLayout = findViewById(R.id.mainactivity);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        Log.w("`````````````````", "---------------------------");
                        // set item as selected to persist highlight
                        menuItem.setChecked(true);
                        // close drawer when item is tapped
//                    mDrawerLayout.closeDrawers();

                        // Add code here to update the UI based on the item selected
                        // For example, swap UI fragments here
                        Log.w("item======", String.valueOf(menuItem.getItemId()));
                        switch (menuItem.getItemId()) {
                            case R.id.dashboard2:
                                // User chose the "Settings" item, show the app settings UI...
                                Intent main = new Intent(context, MainActivity.class);
                                startActivity(main);
                                return true;
                            case R.id.action_settings:
                                // User chose the "Settings" item, show the app settings UI...
                                Intent settings = new Intent(context, Settings.class);
                                startActivity(settings);
                                return true;
                            case R.id.signin :
                                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                                startActivityForResult(signInIntent, RC_SIGN_IN);
                                return true;
                            case R.id.signOut:
                                mGoogleSignInClient.signOut();
                                showLogin();
                                return true;
                            case R.id.year:
                                Intent year_activity = new Intent(context, Years.class);
                                startActivity(year_activity);
                                return true;
                            case R.id.month:
                                Intent month_activity = new Intent(context, Months.class);
                                startActivity(month_activity);
                                return true;
                            case R.id.day:
                                Intent day_activity = new Intent(context, Days.class);
                                startActivity(day_activity);
                                return true;
                        }

                        return true;
                    }
                });

        this.display();
    }

    public void display() {

    }

    protected   void showLogin() {
        NavigationView navigationView = findViewById(R.id.nav_view);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.signin).setVisible(true);
        nav_Menu.findItem(R.id.signOut).setVisible(false);

    }

    protected  void showLogout() {
        dbHelper.syncFromServer(database, account);

        NavigationView navigationView = findViewById(R.id.nav_view);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.signin).setVisible(false);
        nav_Menu.findItem(R.id.signOut).setVisible(true);
    }

    @Override
    public void onStart()
    {
        super.onStart();
        // Check for existing Google Sign In account, if the user is already signed in
        // the GoogleSignInAccount will be non-null.
        account = GoogleSignIn.getLastSignedInAccount(this);
        if (account == null) {
            showLogin();
        } else {
            showLogout();
        }
        display();
    }

    @Override
    protected void onResume()
    {
        display();
        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            // Signed in successfully, show authenticated UI.
            showLogout();
            display();
            dbHelper.syncFromServer(database, account);
            Log.w("tag2:", account.getId());

        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("tag2:", "message:" + e.getMessage());
            display();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        mDrawerLayout.openDrawer(GravityCompat.START);
        return true;
    }

    protected String timeConvert(int time) {
        Log.e("time::::", String.valueOf(time));
        int day = time / 24 / 60;
        int hour = time/60%24;
        int min = time%60;
        String timeRet = day + " ";
        if (day > 1) {
            timeRet += String.valueOf(context.getText(R.string.days));
        } else {
            timeRet += String.valueOf(context.getText(R.string.day_min));
        }
        timeRet += ", " + hour + " ";
        if (hour > 1) {
            timeRet += String.valueOf(context.getText(R.string.hours));
        } else {
            timeRet += String.valueOf(context.getText(R.string.hour));
        }
        timeRet += ", " + String.valueOf(context.getText(R.string.and)) + " " + min + " ";
        if (min > 1) {
            timeRet += String.valueOf(context.getText(R.string.minutes));
        } else {
            timeRet += String.valueOf(context.getText(R.string.minute));
        }
        return timeRet;
    }
}