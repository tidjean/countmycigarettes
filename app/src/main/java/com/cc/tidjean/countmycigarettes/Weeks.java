package com.cc.tidjean.countmycigarettes;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.StaticLabelsFormatter;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Weeks extends AppCompatActivity {

    private SQLiteDatabase database;
    private Context context;
    private dataSql dbHelper;

    List<BarGraphSeries> seriesList = new ArrayList<BarGraphSeries>();
    List<DataDay> listData = new ArrayList<DataDay>();
    List<dataSql.Cigarette> list = new ArrayList();
    List<List<String>> listCigarettes = new ArrayList<List<String>>();
    List<String> dates = new ArrayList<>();
    LayoutInflater inflater;	//Used to create individual pages
    ViewPager vp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.weeks);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.home);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        this.context = getApplicationContext();
        this.dbHelper = new dataSql(context);
        this.database = dbHelper.getWritableDatabase();

        displayCount();

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //store the time into the database
                ContentValues values = new ContentValues();
                Timestamp time = new Timestamp(System.currentTimeMillis());
                values.put("time", time.toString());
                values.put("timestamp", time.getTime());
                database.insert("cigarette", null, values);

                //display confirmation message
                CharSequence text = context.getText(R.string.added);
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();

                displayCount();

            }
        });

        //get an inflater to be used to create single pages
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //Reference ViewPager defined in activity
        vp=(ViewPager)findViewById(R.id.viewpager);
        //set the adapter that will create the individual pages
        vp.setAdapter(new MyPagesAdapter());
        vp.setCurrentItem(seriesList.size() - 1);


        final Button day = (Button) findViewById(R.id.day);
        day.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent home = new Intent(context, Days.class);
                startActivity(home);
            }
        });
        final Button month = (Button) findViewById(R.id.month);
        month.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent home = new Intent(context, Months.class);
                startActivity(home);
            }
        });
        final Button year = (Button) findViewById(R.id.year);
        year.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent home = new Intent(context, Years.class);
                startActivity(home);
            }
        });
    }

    @Override
    protected void onResume()
    {
        displayCount();
        super.onResume();
    }
    class MyPagesAdapter extends PagerAdapter {
        @Override
        public int getCount() {
            //Return total pages, here one for each data item
            return seriesList.size();
        }

        //Create the given page (indicated by position)
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View page = inflater.inflate(R.layout.page, null);
            GraphView graph = (GraphView) page.findViewById(R.id.graph);
            graph.addSeries(seriesList.get(position));
            graph.getViewport().setXAxisBoundsManual(true);
            graph.getViewport().setMaxX(24);
            // set manual Y bounds
            StaticLabelsFormatter staticLabelsFormatter = new StaticLabelsFormatter(graph);
            staticLabelsFormatter.setHorizontalLabels(new String[] {"12","2","4","6","8","10", "12am", "2", "4", "6", "8", "10", "12"});
            graph.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter);
            //Add the page to the front of the queue
            ((ViewPager) container).addView(page, 0);

            TextView count = (TextView) page.findViewById(R.id.countDay);
            count.setText(context.getText(R.string.totalSmoke) + ":" + listData.get(position).count);

//            TextView diffMin = (TextView) page.findViewById(R.id.diffMin);
//            Double min = listData.get(position).diffMin / 60000;
//            diffMin.setText(String.format(String.valueOf(context.getText(R.string.shortTime)), new DecimalFormat("##.##").format(min)));
//
//            TextView diffMax = (TextView) page.findViewById(R.id.diffMax);
//            Double max = listData.get(position).diffMax / 60000;
//            diffMax.setText(String.format(String.valueOf(context.getText(R.string.longestTime)), new DecimalFormat("##.##").format(max)));

            TextView date = (TextView) page.findViewById(R.id.date);
            date.setText(dates.get(position));

//            ListView mListView = (ListView) page.findViewById(R.id.ListCigarettes);
//            String[] listItems = new String[listCigarettes.get(position).size()];
//
//            for(int i = 0; i < listCigarettes.get(position).size(); i++){
//                String time = listCigarettes.get(position).get(i);
//                listItems[i] = time;
//            }
//
//            ArrayAdapter adapter = new ArrayAdapter(context, R.layout.cigarette_day, listItems);
//
//            mListView.setAdapter(adapter);

            TextView better = findViewById(R.id.smokemoreorless);
            if (listData.get(position).better) {
                better.setText("(" + String.valueOf(context.getText(R.string.smokemoreweek)) + ")");
            } else {
                better.setText("(" + String.valueOf(context.getText(R.string.smokelesssday)) + ")");
            }

            return page;
        }
        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            //See if object from instantiateItem is related to the given view
            //required by API
            return arg0==(View)arg1;
        }
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((View) object);
            object=null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent home = new Intent(context, MainActivity.class);
                startActivity(home);
                return true;

            case R.id.action_settings:
                // User chose the "Settings" item, show the app settings UI...
                Intent settings = new Intent(context, Settings.class);
                startActivity(settings);
                return true;

            case R.id.dashboard:
                Intent dashboard = new Intent(context, MainActivity.class);
                startActivity(dashboard);
                return true;

            case R.id.year:
                Intent year_activity = new Intent(context, Years.class);
                startActivity(year_activity);
                return true;
            case R.id.month:
                Intent month_activity = new Intent(context, Months.class);
                startActivity(month_activity);
                return true;
            case R.id.day:
                Intent day_activity = new Intent(context, Days.class);
                startActivity(day_activity);
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    public void displayCount() {
        list = dbHelper.getAll(database);
        seriesList = new ArrayList<BarGraphSeries>();
        listData =  new ArrayList<DataDay>();
        listCigarettes = new ArrayList<List<String>>();
        dates = new ArrayList<>();

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

        Date date = new Date();
        String todate = dateFormat.format(date);

        for (int i = 7; i >= 0; i--){
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, - i);
            Date todate1 = cal.getTime();
            this.createSerie(todate1);
            this.createData(todate1);
        }

        //get an inflater to be used to create single pages
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //Reference ViewPager defined in activity
        vp = (ViewPager)findViewById(R.id.viewpager);
        //set the adapter that will create the individual pages
        vp.setAdapter(new MyPagesAdapter());
        vp.setCurrentItem(seriesList.size() - 1);
        vp.refreshDrawableState();
    }

    private List createDay (Date date) {
        String year = new SimpleDateFormat("yyyy").format(date);
        String month = new SimpleDateFormat("MM").format(date);
        String day = new SimpleDateFormat("dd").format(date);


        List dayCount = new ArrayList();
        for (int j = 0; j < 24; j++) {
            dayCount.add(0);
        }
        for(int i = 0; i < list.size(); i++) {
            if (list.get(i).time.startsWith(year + "-" + month + "-" + day)) {
                int hour = Integer.parseInt(list.get(i).time.substring(11, 13));
                int hour1 = (int) dayCount.get(hour);
                dayCount.set(hour,  hour1 + 1);
            }
        }
        return dayCount;
    }

    public void createSerie (Date date) {
        List dayCount = this.createDay(date);
        DataPoint[] datapoints = new DataPoint[24];
        for (int j = 0; j < 24; j++) {
            datapoints[j] = new DataPoint(j, (int) dayCount.get(j));
        }
        BarGraphSeries<DataPoint> series = new BarGraphSeries<>(datapoints);
        seriesList.add(series);
    }

    public void createData (Date date) {
        String year = new SimpleDateFormat("yyyy").format(date);
        String month = new SimpleDateFormat("MM").format(date);
        String day = new SimpleDateFormat("dd").format(date);

        dates.add(year + "-" + month + "-" + day);

        DataDay data = new DataDay();
        List<String> listCigarettesDay = new ArrayList<String>();
        List dayCount = new ArrayList();

        for (int j = 0; j < 24; j++) {
            dayCount.add(0);
        }

        for(int i = 0; i < list.size(); i++) {
            if (list.get(i).time.startsWith(year + "-" + month + "-" + day)) {
                data.count++;

                Timestamp ts = new Timestamp(Long.parseLong(list.get(i).timestamp));
                Date dateTmp = new Date(ts.getTime());
                DateFormat df = new SimpleDateFormat("HH:mm:ss");
                String text = df.format(dateTmp);

                listCigarettesDay.add(text);
                if (data.count > 1) {
                    if (Double.parseDouble(list.get(i).timestamp) - Double.parseDouble(list.get(i-1).timestamp) > data.diffMax) {
                        data.diffMax = Double.parseDouble(list.get(i).timestamp) - Double.parseDouble(list.get(i - 1).timestamp);
                    }
                    if (data.count == 2){
                        data.diffMin = Double.parseDouble(list.get(i).timestamp) - Double.parseDouble(list.get(i - 1).timestamp);
                    }
                    if (Double.parseDouble(list.get(i).timestamp) - Double.parseDouble(list.get(i-1).timestamp) < data.diffMin) {
                        data.diffMin = Double.parseDouble(list.get(i).timestamp) - Double.parseDouble(list.get(i - 1).timestamp);
                    }

                }
            }
        }

        if (listData.size() > 1 && data.count > listData.get(listData.size() - 1).count) {
            data.better = true;
        }

        listCigarettes.add(listCigarettesDay);
        listData.add(data);
    }

    public class DataDay {
        public int count ;
        public Double diffMax;
        public Double diffMin;
        public boolean better ;

        public DataDay () {
            this.count = 0;
            this.diffMin = 0.0;
            this.diffMax = 0.0;
            this.better = false;
        }
    }
}