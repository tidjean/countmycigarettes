package com.cc.tidjean.countmycigarettes;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

public class MainActivity extends AppActivity {
    private int mYear, mMonth, mDay, mHour, mMinute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_main);

        super.onCreate(savedInstanceState);

        Button viewDay = (Button) findViewById(R.id.viewDays);
        viewDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(context, Days.class);
                startActivity(myIntent);
            }
        });

        findViewById(R.id.sign_in_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        });

        findViewById(R.id.signOut).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.w("signout", "onclic");
                mGoogleSignInClient.signOut();
                showLogin();
            }
        });
    }

    @Override
    public void display() {
        ArrayList<dataSql.Cigarette> cigarettes = (ArrayList<dataSql.Cigarette>) dbHelper.getAll(database);
        long diff = 1;

        if (cigarettes.size() > 0) {
            //get the average of cigarette per day
            String start = cigarettes.get(0).timestamp;
            String end = cigarettes.get(cigarettes.size() - 1).timestamp;
            diff = TimeUnit.DAYS.convert(Long.parseLong(end) - Long.parseLong(start), TimeUnit.MILLISECONDS) + 1;
        }

        float average = cigarettes.size() / diff;

        TextView count = (TextView) findViewById(R.id.totalSmoke);
        count.setText(cigarettes.size() + "");

        TextView nextSmoke = (TextView) findViewById(R.id.smokeNextYear);
        nextSmoke.setText(String.format(String.valueOf(context.getText(R.string.willSmoke)), average * 365));

        TextView health = (TextView) findViewById(R.id.healthLost);
        health.setText(String.format(String.valueOf(context.getText(R.string.lostTime)), timeConvert(cigarettes.size() * 11)));

//        TextView healthLose = (TextView) findViewById(R.id.healthWillLose);
//        healthLose.setText(String.format(String.valueOf(context.getText(R.string.willLose)), timeConvert((int) (average * 365 * 11))));

        TextView spent = (TextView) findViewById(R.id.totalSpend);
        TextView spendYear = (TextView) findViewById(R.id.spendNextYear);
        View divspent = findViewById(R.id.dividermoneylose);
        View divwillspent = findViewById(R.id.dividerwillmoneylose);

        //do not display the price calculs if the pack's price is not defined
        if (config.price > 0) {
            spent.setVisibility(View.VISIBLE);
            spendYear.setVisibility(View.VISIBLE);
            divspent.setVisibility(View.VISIBLE);
            divwillspent.setVisibility(View.VISIBLE);
            spent.setText(String.format(String.valueOf(context.getText(R.string.xSpentCigarettes)), (cigarettes.size() * config.price / 20) + config.currency));
            spendYear.setText(String.format(String.valueOf(context.getText(R.string.willSpend)), (average * 365 * config.price / 20) + config.currency));
        } else {
            spent.setVisibility(View.GONE);
            spendYear.setVisibility(View.GONE);
            divspent.setVisibility(View.GONE);
            divwillspent.setVisibility(View.GONE);
        }
    }

    protected void showLogout() {
        findViewById(R.id.signOut).setVisibility(View.VISIBLE);
        findViewById(R.id.textSign).setVisibility(View.GONE);
        findViewById(R.id.sign_in_button).setVisibility(View.GONE);
        super.showLogout();
    }

    protected void showLogin() {
        findViewById(R.id.signOut).setVisibility(View.GONE);
        findViewById(R.id.textSign).setVisibility(View.VISIBLE);
        findViewById(R.id.sign_in_button).setVisibility(View.VISIBLE);
        super.showLogin();
    }
}