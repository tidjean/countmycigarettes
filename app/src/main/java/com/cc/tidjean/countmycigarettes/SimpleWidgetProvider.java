package com.cc.tidjean.countmycigarettes;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.widget.RemoteViews;
import android.widget.Toast;

import java.sql.Timestamp;

/**
 * Created by tidjean on 25/03/17.
 */

public class SimpleWidgetProvider extends AppWidgetProvider {
    private SQLiteDatabase database;
    private Context context2;
    private dataSql dbHelper;
    private boolean first = false;
    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {

        final int count = appWidgetIds.length;

        this.context2 = context;

        for (int i = 0; i < count; i++) {
            int widgetId = appWidgetIds[i];
            RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
                    R.layout.widget);

            Intent intent = new Intent(context, SimpleWidgetProvider.class);
            intent.setAction("clickAdd");

            PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
                    0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            remoteViews.setOnClickPendingIntent(R.id.actionButton, pendingIntent);
            appWidgetManager.updateAppWidget(widgetId, remoteViews);

        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);//add this line
        if (intent.getAction() == "clickAdd") {
            this.dbHelper = new dataSql(context);
            this.database = dbHelper.getWritableDatabase();

            //store the time into the database
            ContentValues values = new ContentValues();
            Timestamp time = new Timestamp(System.currentTimeMillis());
            values.put("time", time.toString());
            values.put("timestamp", time.getTime());
            database.insert("cigarette", null, values);

            //display confirmation message
            CharSequence text = "Cigarette added.";
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }
    }

}